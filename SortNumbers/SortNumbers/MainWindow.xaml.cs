﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Windows;

namespace SortNumbers
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private string filePath;
        private string fileName;
        private string purePath;
        private BackgroundWorker worker;
        public MainWindow()
        {
            InitializeComponent();

            worker = new BackgroundWorker();
            worker.DoWork += worker_DoWork;
            worker.RunWorkerCompleted += worker_RunWorkerCompleted;
            worker.WorkerReportsProgress = true;
            worker.WorkerSupportsCancellation = true;
        }

        /// <summary>
        /// Handles the Click event of the btnOpenFile control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnOpenFile_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "Abb files (*.abb)|*.abb";

            if (openFileDialog.ShowDialog() == true)
            {
                txbFilePath.Text = openFileDialog.FileName;
                filePath = txbFilePath.Text;
                fileName = openFileDialog.SafeFileName;
                purePath = filePath.Replace(fileName, "");
            }
        }

        /// <summary>
        /// Handles the Click event of the btnCancelSort control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnCancelSort_Click(object sender, RoutedEventArgs e)
        {
            btnSort.IsEnabled = true;;
            btnCancelSort.IsEnabled = false;
            worker.CancelAsync();
        }
        /// <summary>
        /// Handles the DoWork event of the worker control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="DoWorkEventArgs"/> instance containing the event data.</param>
        private void worker_DoWork(object sender, DoWorkEventArgs e)
        {
            BackgroundWorker worker = sender as BackgroundWorker;
                e.Cancel = Split(filePath);
            if (!e.Cancel)
                e.Cancel = SortTheChunks();
            if (!e.Cancel)
                e.Cancel = MergeTheChunks();
        }

        /// <summary>
        /// Handles the Click event of the btnSort control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void btnSort_Click(object sender, RoutedEventArgs e)
        {
            txbStatus.Text = "Processing...";
            if (!worker.IsBusy)
                worker.RunWorkerAsync();
        }

        /// <summary>
        /// Handles the RunWorkerCompleted event of the worker control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RunWorkerCompletedEventArgs"/> instance containing the event data.</param>
        private void worker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Cancelled)
            {
                txbStatus.Text = "Cancelled";
            }
            else
            {
                txbStatus.Text = "Completed";
            }
            btnSort.IsEnabled = true;
            btnCancelSort.IsEnabled = false;
        }

        /// <summary>
        /// Splitting huge file into small ones
        /// </summary>
        /// <param name="file">The file.</param>
        /// <returns></returns>
        public bool Split(string file)
        {
            int split_num = 1;
            bool cancel = false; 
            StreamWriter sw = new StreamWriter(
              string.Format(purePath+"split{0:d5}.dat", split_num));
            using (StreamReader sr = new StreamReader(file))
            {
                while (sr.Peek() >= 0)
                {
                    if (worker.CancellationPending)
                    {
                        cancel =  true;
                        break;
                    }
                    sw.WriteLine(sr.ReadLine());
                      if (sw.BaseStream.Length > 100000000 && sr.Peek() >= 0)
                    {
                        sw.Close();
                        split_num++;
                        sw = new StreamWriter(
                          string.Format(purePath+"split{0:d5}.dat", split_num));
                    }
                }
            }
            sw.Close();
            return cancel;
        }

        /// <summary>
        /// Sorts the chunks.
        /// </summary>
        /// <returns></returns>
        public bool SortTheChunks()
        {
            
            foreach (string path in Directory.GetFiles(purePath, "split*.dat"))
            {
                if (worker.CancellationPending)
                {
                    return true;
                }
                double[] contents =
                File.ReadAllLines(path)
                    .Select(s => double.Parse(s)).ToArray();
                Array.Sort(contents);
                string newpath = path.Replace("split", "sorted");
                File.WriteAllLines(newpath, contents.Select(s => s.ToString()));
                File.Delete(path);
                contents = null;
                GC.Collect();
            }
            return false;
        }

        /// <summary>
        /// Merges the chunks.
        /// </summary>
        /// <returns></returns>
        public bool MergeTheChunks()
        {
            string[] paths = Directory.GetFiles(purePath, "sorted*.dat");
            int chunks = paths.Length; 
            int recordsize = 100; 
            int maxusage = 500000000;
            int buffersize = maxusage / chunks; 
            double recordoverhead = 7.5; 
            int bufferlen = (int)(buffersize / recordsize /
              recordoverhead); 
            StreamReader[] readers = new StreamReader[chunks];
            for (int i = 0; i < chunks; i++)
            {
                if (worker.CancellationPending)
                {
                    return true;
                }
                readers[i] = new StreamReader(paths[i]);
            }

            Queue<string>[] queues = new Queue<string>[chunks];
            for (int i = 0; i < chunks; i++)
            {
                if (worker.CancellationPending)
                {
                    return true;
                }
                queues[i] = new Queue<string>(bufferlen);
            }
            for (int i = 0; i < chunks; i++)
            {
                if (worker.CancellationPending)
                {
                    return true;
                }
                LoadQueue(queues[i], readers[i], bufferlen);
            }

            StreamWriter sw = new StreamWriter(filePath, false);
            bool done = false;
            int lowest_index, j;
            string lowest_value;
            while (!done)
            {
                if (worker.CancellationPending)
                {
                    return true;
                }

                lowest_index = -1;
                lowest_value = "";
                for (j = 0; j < chunks; j++)
                {
                    if (worker.CancellationPending)
                    {
                        return true;
                    }

                    if (queues[j] != null)
                    {
                        if (lowest_index < 0 ||
                          String.CompareOrdinal(
                            queues[j].Peek(), lowest_value) < 0)
                        {
                            lowest_index = j;
                            lowest_value = queues[j].Peek();
                        }
                    }
                }

                if (lowest_index == -1) { done = true; break; }
                sw.WriteLine(lowest_value);

                queues[lowest_index].Dequeue();
                if (queues[lowest_index].Count == 0)
                {
                    LoadQueue(queues[lowest_index],
                      readers[lowest_index], bufferlen);
                    if (queues[lowest_index].Count == 0)
                    {
                        queues[lowest_index] = null;
                    }
                }
            }
            sw.Close();
            for (int i = 0; i < chunks; i++)
            {
                readers[i].Close();
                File.Delete(paths[i]);
            }
            return false;
        }

        /// <summary>
        /// Loads the queue.
        /// </summary>
        /// <param name="queue">The queue.</param>
        /// <param name="file">The file.</param>
        /// <param name="records">The records.</param>
        public static void LoadQueue(Queue<string> queue,
          StreamReader file, int records)
        {
            for (int i = 0; i < records; i++)
            {
                if (file.Peek() < 0) break;
                queue.Enqueue(file.ReadLine());
            }
        }
    }
}
