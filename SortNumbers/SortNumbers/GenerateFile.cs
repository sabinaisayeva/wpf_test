﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SortNumbers
{
    public static class GenerateFile
    {
        public static void GenerateBigFile()
        {

            using (System.IO.StreamWriter file =new System.IO.StreamWriter(@"C:\testfile.abb"))
            {
                for (long i = 10000000; i >-50; i--)
                {
                    file.WriteLine(NextDouble(i, i + 5500));
                }
            }
        }

        public static double NextDouble(double min, double max)
        {
            System.Random rng = new System.Random();
            return min + (rng.NextDouble() * (max - min));
        }
    }
}
